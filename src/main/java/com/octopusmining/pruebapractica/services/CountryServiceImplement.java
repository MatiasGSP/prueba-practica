package com.octopusmining.pruebapractica.services;

import com.octopusmining.pruebapractica.model.Country;
import com.octopusmining.pruebapractica.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryServiceImplement implements CountryService {

    @Autowired
    private CountryRepository countryRepo;

    public Country findCountryByShortname(String shortname){
        return countryRepo.findCountryByShortname(shortname).orElse(null);
    }
}
