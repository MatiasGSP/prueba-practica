package com.octopusmining.pruebapractica.services;

import com.octopusmining.pruebapractica.model.Currency;

public interface CurrencyService {

    public Currency findCurrencyByShortname(String shortname);

}
