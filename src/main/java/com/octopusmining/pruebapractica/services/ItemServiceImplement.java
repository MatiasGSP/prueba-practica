package com.octopusmining.pruebapractica.services;

import com.octopusmining.pruebapractica.model.Category;
import com.octopusmining.pruebapractica.model.Country;
import com.octopusmining.pruebapractica.model.Currency;
import com.octopusmining.pruebapractica.model.Item;
import com.octopusmining.pruebapractica.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

@Service
public class ItemServiceImplement implements ItemService {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private ItemRepository itemRepo;

    @Autowired
    private ItemService itemService;


    @Override
    public Item findItemById(Integer id) {
        return itemRepo.findById(id).orElse(null);
    }

    /**
     *
     * @param itemId - ID del Item a actualizar.
     * @param updateData - Datos del Item a actualizar, en formato JSON. Se obtienen directamente desde el formato JSON a objeto Hashtable para mayor comodidad en el manejo de datos.
     * @return
     */

    @Override
    public Item updateItem(Integer itemId, Hashtable updateData) {
        Date modifiedAt = new Date();
        Item itemToUpdate = findItemById(itemId);
        Date createdAt = new Date();
        double price = (Integer)(updateData.get("price"));
        itemToUpdate.setTitle((String)updateData.get("title"));
        itemToUpdate.setPrice(price);
        itemToUpdate.setCurrency(currencyService.findCurrencyByShortname((String)(updateData.get("currency"))));
        itemToUpdate.setCountry(countryService.findCountryByShortname((String)(updateData.get("country"))));
        itemToUpdate.setModifiedAt(modifiedAt);
        itemRepo.save(itemToUpdate);
        return itemToUpdate;
    }

    /**
     *
     * @param id - ID del Item a borrar.
     * @return - Retorna Hashtable con Item borrado y mensaje de éxito, en caso de encontrarlo.
     */

    @Override
    public Hashtable deleteItem(@PathVariable Integer id){
        Item itemToDelete = findItemById(id);
        itemRepo.delete(itemToDelete);
        Hashtable response = new Hashtable();
        response.put(itemToDelete,"Item successfully deleted from database.");
        return response;
    }

    /**
     *
     * @param creationData - Hashtable con datos del objeto Item a crear.
     * @return - Retorna Hashtable con ID y título del objeto creado, junto a un mensaje de éxito.
     */

    @Override
    public Hashtable createItem(Hashtable creationData){
        Item itemToCreate = new Item();
        Date createdAt = new Date();
        Hashtable response = new Hashtable();

        double price = (Integer)(creationData.get("price"));
        itemToCreate.setTitle((String)creationData.get("title"));
        itemToCreate.setPrice(price);
        itemToCreate.setCurrency(currencyService.findCurrencyByShortname((String)(creationData.get("currency"))));
        itemToCreate.setCountry(countryService.findCountryByShortname((String)(creationData.get("country"))));
        itemToCreate.setCategory(categoryService.findCategoryById((Integer)(creationData.get("category"))));
        itemToCreate.setCreatedAt(createdAt);
        itemToCreate.setSymbol(((String)creationData.get("title")).charAt(0));
        int createdItemIndex=itemRepo.save(itemToCreate).getId();
        response.put("Item created. Its ID is",createdItemIndex);

        return response;
    }

}
