package com.octopusmining.pruebapractica.services;

import com.octopusmining.pruebapractica.model.Country;

public interface CountryService {

    public Country findCountryByShortname(String shortname);

}
