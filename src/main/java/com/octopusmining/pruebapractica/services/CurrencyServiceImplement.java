package com.octopusmining.pruebapractica.services;

import com.octopusmining.pruebapractica.model.Currency;
import com.octopusmining.pruebapractica.repositories.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyServiceImplement implements CurrencyService{

    @Autowired
    private CurrencyRepository currencyRepo;

    public Currency findCurrencyByShortname(String shortname){
        return currencyRepo.findCurrencyByShortname(shortname).orElse(null);
    }
}
