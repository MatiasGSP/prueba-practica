package com.octopusmining.pruebapractica.services;

import com.octopusmining.pruebapractica.model.Category;

public interface CategoryService {

    public Category findCategoryById(Integer id);

}
