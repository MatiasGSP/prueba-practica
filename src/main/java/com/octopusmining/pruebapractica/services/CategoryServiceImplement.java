package com.octopusmining.pruebapractica.services;

import com.octopusmining.pruebapractica.model.Category;
import com.octopusmining.pruebapractica.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImplement implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepo;

    public Category findCategoryById(Integer id){
        return categoryRepo.findById(id).orElse(null);
    }

}
