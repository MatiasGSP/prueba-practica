package com.octopusmining.pruebapractica.services;

import com.octopusmining.pruebapractica.model.Item;

import java.util.Hashtable;
import java.util.List;

public interface ItemService {

    public Item findItemById(Integer id);
    public Item updateItem(Integer itemId,  Hashtable itemUpdate);
    public Hashtable deleteItem(Integer itemId);
    public Hashtable createItem(Hashtable item);

}
