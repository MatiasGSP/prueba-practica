package com.octopusmining.pruebapractica.controllers;
import com.octopusmining.pruebapractica.model.Item;
import com.octopusmining.pruebapractica.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.*;

@RestController
@RequestMapping("/api/item")
public class ItemController {

    @Autowired
    private ItemService itemServ;

    /**
     *
     * @param id - Parámetro según el cual, busca el objeto Item.
     * @return Retorna el objeto si es que lo encuentra, o null.
     */

    @GetMapping("/{id}")
    public Item findItemById(@PathVariable Integer id){
        Item itemFound = null;
        if(itemServ.findItemById(id)!=null)
            itemFound = itemServ.findItemById(id);
        return itemFound;
    }

    /**
     *
     * @param id - Parámetro con el que busca el Item a actualizar.
     * @param updateData - Datos que actualiza del item - obtenido desde BODY.
     * @return - Retorna el item actualizado, o null.
     */

    @PutMapping("/{id}")
    public Item updateItem(@PathVariable Integer id,@RequestBody Hashtable updateData){
        Item itemFound = null;
        if(itemServ.findItemById(id)!=null)
            return itemServ.updateItem(id,updateData);
        return itemFound;
    }


    /**
     *
     * @param id - Parámetro según el cual, elimina una entrada Item.
     * @return - Retorna el ID y título del objeto borrado, junto a un mensaje de éxíto, en formato Hashtable.
     */

    @DeleteMapping("/{id}")
    public Hashtable deleteItem(@PathVariable Integer id){
        Hashtable response = new Hashtable();
        if(itemServ.findItemById(id)!=null) {
            response=itemServ.deleteItem(id);
        }else{
            response.put(("ID not found"),("Could not delete Item"));
        }
        return response;
    }

    /**
     *
     * @param createData - Body correspondiente al JSON con parámetros de creación para objeto Item.
     * @return - Retorna Hashtable con ID del Item, para su visualización en formato JSON.
     */

    @PostMapping()
    public Hashtable createItem(@RequestBody Hashtable createData){
        return itemServ.createItem(createData);
    }

}
