/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.octopusmining.pruebapractica.repositories;

import com.octopusmining.pruebapractica.model.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *
 * @author USUARIO ASUS
 */
@Repository
public interface CountryRepository extends CrudRepository<Country, Integer> {

    Optional<Country> findCountryByShortname(String shortname);
    
}
