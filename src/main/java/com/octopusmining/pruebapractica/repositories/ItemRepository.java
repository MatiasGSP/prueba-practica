/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.octopusmining.pruebapractica.repositories;

import com.octopusmining.pruebapractica.model.Category;
import com.octopusmining.pruebapractica.model.Country;
import com.octopusmining.pruebapractica.model.Currency;
import com.octopusmining.pruebapractica.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author USUARIO ASUS
 */
@Repository
public interface ItemRepository extends CrudRepository<Item, Integer> {

}
