/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.octopusmining.pruebapractica.repositories;

import com.octopusmining.pruebapractica.model.Currency;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *
 * @author USUARIO ASUS
 */
@Repository
public interface CurrencyRepository extends CrudRepository<Currency, Integer> {

    Optional<Currency> findCurrencyByShortname(String shortname);
    
}
